
$("#card_1").click(function(ce){ //picks up tap
	flip(ce);
	console.log("left: " + this.offsetLeft);
	console.log("top: " + this.offsetTop);
});

$("#card_1").on("touchstart", function(tse){	
	$("body").css("background-color", "darkred");
	//$("output").html("tse");
});

$("#card_1").on("touchmove", function(tme){	
	var stuff  = {},
		html = '',
		dim = 0;
	$("body").css("background-color", "darkgreen");
	tme.preventDefault();
	var tee = tme.originalEvent.changedTouches[0];//first finger only
	for(stuff in tee){
		if(stuff === "clientX" || stuff === "clientY"){
			if(stuff === "clientX"){
				dim = tee[stuff] - this.offsetLeft;
				if(dim < 0){
					dim = 0;
				}
				if(dim > this.offsetWidth){
					dim = this.offsetWidth;
				}
			}else{
				dim = tee[stuff] - this.offsetTop;
				if(dim < 0){
					dim = 0;
				}
				if(dim > this.offsetHeight){
					dim = this.offsetHeight;
				}
			}
			html += stuff + ":: " + dim + "<br />";
			/*
				find a way to move this left
				should track the touch on the holder and then move the item within the holder.
				If pinch trigger englarge image
			*/
		}
	}
	html += "the position top is: " + this.offsetTop + "<br />";
	html += "the position left is: " + this.offsetLeft;
	$("output").html(html)
});

/**
We can look for webkitCompassHeading
*/
$(window).on("deviceorientation", function(dme){
	var html = "hey this moved:",
		stuff = {},
		tee = dme.originalEvent,
		$card = $("body");
	for(stuff in tee){
		html += stuff + ":: " + tee[stuff] + "<br />";
	}
	$("output").html(html);
	if(parseInt(tee.webkitCompassHeading, 10) < 10 || parseInt(tee.webkitCompassHeading, 10) > 350){
		$card.css("background-color", "red");//"-webkit-transform": "rotateY(10deg)");
	}else{
		$card.css("background-color", "yellow");
	}
	
});

$("#card_1").on("touchend", function(tee){	
	var html = '',
		stuff = {};
	
	$("body").css("background-color", "darkorange");
	
	/*for(stuff in tee){
		html += stuff + ":: " + tee[stuff] + "<br />";
	}
	$("output").html(html)
	*/
});

function flip(tap_event) {
	var el = tap_event.currentTarget;
	el.className = (el.className == "card") ? "card_flipped" : "card";
}
